﻿#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include <windows.h>
#include <profileapi.h>

#define base_t int32_t
#define BASE_MAX INT32_MAX
#define index_t base_t
#define timer_result int32_t

index_t search_binary(base_t item, base_t* array, index_t array_size)
{
	index_t left = 0;
	index_t right = array_size - 1;

	index_t center = (left + right) / 2;

	index_t result = -1;

	while (left <= right)
	{
		if (array[center] < item)
		{
			left = center + 1;
		}
		else if (array[center] == item)
		{
			result = center;
			break;
		}
		else {
			right = center - 1;
		}
		center = (left + right) / 2;
	}

	return result;
}

index_t search_linner(base_t item, base_t* array, index_t array_size)
{
    index_t result = -1;

	for (index_t i = 0; i < array_size; i++)
	{
		if (array[i] == item) {
			result = i;
			break;
		}
	}
	return result;
}

base_t* gen_serial_array(index_t size)
{
	base_t *array = malloc(size * sizeof(base_t));

	for (index_t i = 0; i < size; i += 1)
	{
		array[i] = i;
	}

	return array;
}


base_t* randomize_array(base_t* array, index_t size)
{
	for (index_t i = 0; i < size; i += 1)
	{
		array[i] = rand() % BASE_MAX;
	}
	return array;
}

int compare(const void *x, const void* y)
{
	return (*(base_t*)x - *(base_t*)y);
}
__inline void sort_array_quick(base_t* array, index_t array_size)
{
	qsort(array, array_size, sizeof(base_t), compare);
}

LARGE_INTEGER li;
double PCFreq = 0.0;
__int64 CounterStart = 0;

void start_timer()
{
	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}
timer_result get_timer()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return (timer_result)((li.QuadPart - CounterStart) / PCFreq);
}

int main()
{

	uint32_t raunds = 20;
	uint32_t raund_coef = 2;
	uint32_t iteration_count = 100;
	QueryPerformanceFrequency(&li);
	PCFreq = (double)(li.QuadPart / 1000000.0);

	srand(time(0));

	base_t* array = NULL;
	base_t array_size = 1;
	base_t search_item = 0;
	index_t index = -1;
	printf("raund;size;serial_liner;serial_binary;random_liner;random_binary;qsort;random_binary_qsort;\n");
	for (size_t i = 0; i < raunds; i += 1)
	{
		timer_result avg_search_linner_serial_result = 0;
		timer_result avg_search_binary_serial_result = 0;
		timer_result avg_search_linner_randomize_result = 0;
		timer_result avg_search_binary_randomize = 0;
		timer_result avg_quick_sort = 0;
		timer_result avg_search_binary_randomize_quick_result = 0;

		array_size = array_size * raund_coef;

		for (size_t iteration = 0; iteration < iteration_count; iteration++)
		{	
			array = gen_serial_array(array_size);

			search_item = array[rand() % array_size];

			start_timer();
			index = search_linner(search_item, array, array_size);
			timer_result search_linner_serial_result = get_timer();

			start_timer();
			index = search_binary(search_item, array, array_size);
			timer_result search_binary_serial_result = get_timer();

			randomize_array(array, array_size);
			search_item = array[rand() % array_size];

			start_timer();
			index = search_linner(search_item, array, array_size);
			timer_result search_linner_randomize_result = get_timer();

			start_timer();
			sort_array_quick(array, array_size);
			timer_result quick_sort = get_timer();
			index = search_binary(search_item, array, array_size);
			timer_result search_binary_randomize_quick_result = get_timer();

			free(array);

			timer_result search_binary_randomize = search_binary_randomize_quick_result - quick_sort;


			if (iteration == 0)
			{
				avg_search_linner_serial_result = search_linner_serial_result;
				avg_search_binary_serial_result = search_binary_serial_result;
				avg_search_linner_randomize_result = search_linner_randomize_result;
				avg_search_binary_randomize = search_binary_randomize;
				avg_quick_sort = quick_sort;
				avg_search_binary_randomize_quick_result = search_binary_randomize_quick_result;
			}
			else
			{
				avg_search_linner_serial_result = (avg_search_linner_serial_result + search_linner_serial_result) / 2;
				avg_search_binary_serial_result = (avg_search_binary_serial_result + search_binary_serial_result) / 2;
				avg_search_linner_randomize_result = (avg_search_linner_randomize_result + search_linner_randomize_result) / 2;
				avg_search_binary_randomize = (avg_search_binary_randomize + search_binary_randomize) / 2;
				avg_quick_sort = (avg_quick_sort + quick_sort) / 2;
				avg_search_binary_randomize_quick_result = (avg_search_binary_randomize_quick_result + search_binary_randomize_quick_result) / 2;
			}
		}
		printf("%d;%d;%d;%d;%d;%d;%d;%d;\n",
			i + 1,
			array_size,
			avg_search_linner_serial_result,
			avg_search_binary_serial_result,
			avg_search_linner_randomize_result,
			avg_search_binary_randomize,
			avg_quick_sort,
			avg_search_binary_randomize_quick_result
		);
	}
	

    return 0;
}